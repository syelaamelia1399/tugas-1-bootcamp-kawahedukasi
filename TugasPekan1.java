import java.util.Scanner;

public class TugasPekan1 {
    public static void main(String[] args) {
        try {
            System.out.println("Menu Program : 1.Identitas, 2.Kalkulator, 3.Perbandingan");

            Scanner menuProgram = new Scanner(System.in);
            System.out.print("Masukkan pilihan menu program dengan angka : ");
            int angkaProgram = menuProgram.nextInt();

            if (angkaProgram == 1) {
                System.out.println("Nama saya adalah Syela Rizki Amelia");
                System.out.println("Alasan saya ingin menjadi back-end developer adalah karena Saya ingin berkarir di bidang IT yaitu Back-End Developer");
                System.out.println("Ekspektasi saya setelah mengikuti bootcamp ini adalah memiliki project untuk menjadi bekal saya untuk berkarir di bidang IT");
            } else if (angkaProgram == 2) {
                try {
                    System.out.println("Menu Kalkulator : 1.Penjumlahan, 2.Pengurangan, 3.Perkalian, 4.Pembagian, 5.Sisa Bagi");
                    
                    Scanner menuKalkulator = new Scanner(System.in);
                    System.out.print("Masukkan pilihan menu kalkulator dengan angka : ");
                    int angkaKalkulator = menuKalkulator.nextInt();

                    switch (angkaKalkulator) {
                        case 1:
                            Scanner tambahPertama = new Scanner(System.in);
                            System.out.print("Masukkan angka pertama : ");
                            int angkaTambahPertama = tambahPertama.nextInt();
                
                            Scanner tambahKedua = new Scanner(System.in);
                            System.out.print("Masukkan angka kedua : ");
                            int angkaTambahKedua = tambahKedua.nextInt();

                            int penjumlahan = angkaTambahPertama + angkaTambahKedua;
                            System.out.println("Angka " + angkaTambahPertama + " ditambah dengan " + angkaTambahKedua + " sama dengan " + penjumlahan);
                            
                            tambahPertama.close();
                            tambahKedua.close();
                            break;
                        case 2:
                            Scanner kurangPertama = new Scanner(System.in);
                            System.out.print("Masukkan angka pertama : ");
                            int angkaKurangPertama = kurangPertama.nextInt();
                
                            Scanner kurangKedua = new Scanner(System.in);
                            System.out.print("Masukkan angka kedua : ");
                            int angkaKurangKedua = kurangKedua.nextInt();

                            int pengurangan = angkaKurangPertama - angkaKurangKedua;
                            System.out.println("Angka " + angkaKurangPertama + " dikurangi dengan " + angkaKurangKedua + " sama dengan " + pengurangan);
                            
                            kurangPertama.close();
                            kurangKedua.close();
                            break;
                        case 3:
                            Scanner kaliPertama = new Scanner(System.in);
                            System.out.print("Masukkan angka pertama : ");
                            int angkaKaliPertama = kaliPertama.nextInt();
                
                            Scanner kaliKedua = new Scanner(System.in);
                            System.out.print("Masukkan angka kedua : ");
                            int angkaKaliKedua = kaliKedua.nextInt();

                            int perkalian = angkaKaliPertama * angkaKaliKedua;
                            System.out.println("Angka " + angkaKaliPertama + " dikali dengan " + angkaKaliKedua + " sama dengan " + perkalian);
                            
                            kaliPertama.close();
                            kaliKedua.close();
                            break;
                        case 4:
                            Scanner bagiPertama = new Scanner(System.in);
                            System.out.print("Masukkan angka pertama : ");
                            int angkaBagiPertama = bagiPertama.nextInt();
                
                            Scanner bagiKedua = new Scanner(System.in);
                            System.out.print("Masukkan angka kedua : ");
                            int angkaBagiKedua = bagiKedua.nextInt();

                            int pembagian = angkaBagiPertama / angkaBagiKedua;
                            System.out.println("Angka " + angkaBagiPertama + " dibagi dengan " + angkaBagiKedua + " sama dengan " + pembagian);
                            
                            bagiPertama.close();
                            bagiKedua.close();
                            break;
                        case 5:
                            Scanner modPertama = new Scanner(System.in);
                            System.out.print("Masukkan angka pertama : ");
                            int angkaModPertama = modPertama.nextInt();
                
                            Scanner modKedua = new Scanner(System.in);
                            System.out.print("Masukkan angka kedua : ");
                            int angkaModKedua = modKedua.nextInt();

                            int sisaBagi = angkaModPertama % angkaModKedua;
                            System.out.println(angkaModPertama + " modulus " + angkaModKedua + " sama dengan " + sisaBagi);
                            
                            modPertama.close();
                            modKedua.close();
                            break;
                        default :
                            System.out.println("Pilihan menu kalkulator dengan angka " + angkaKalkulator + " tidak ada");
                    }

                    menuKalkulator.close();

                } catch (Exception e) {
                    System.out.println("Mohon masukkan menu kalkulator angka dengan angka antara 1-5");
                }

            } else if (angkaProgram == 3) {
                Scanner bandingPertama = new Scanner(System.in);
                System.out.print("Masukkan angka pertama : ");
                int angkaBandingPertama = bandingPertama.nextInt();

                Scanner bandingKedua = new Scanner(System.in);
                System.out.print("Masukkan angka kedua : ");
                int angkaBandingKedua = bandingKedua.nextInt();

                if (angkaBandingPertama > angkaBandingKedua) {
                    System.out.println(angkaBandingPertama + " lebih besar dari " + angkaBandingKedua);
                } else if (angkaBandingPertama < angkaBandingKedua) {
                    System.out.println(angkaBandingPertama + " lebih kecil dari " + angkaBandingKedua);
                } else {
                    System.out.println(angkaBandingPertama + " sama dengan " + angkaBandingKedua);
                }

                bandingPertama.close();
                bandingKedua.close();
            } else {
                System.out.println("Pilihan menu program dengan angka " + angkaProgram + " tidak ada");
            }

            menuProgram.close();

        } catch (Exception e) {
            System.out.println("Mohon masukkan menu program dengan angka antara 1-3");
        }
        
    }
}